import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams, useRouteMatch} from "react-router-dom";
import {loadSwDetailData} from "../actions/sw.actions";

const LeftColumn = () => {
    const data = useSelector(state => state.sw);
    const dispatch = useDispatch();

    let { url } = useRouteMatch();

    useEffect(() => {
        const slug = url.split('/')[1],
            id = url.split('/')[2];
        dispatch(loadSwDetailData(slug, id));
    }, []);


    return (
        <div className="left">
            <div className="block">
                <h1>{data.name? data.name: data.title}</h1>
                <hr/>
                    <div className="detail active">
                        {
                            Object.entries(data).map(([key, value]) => {
                                return <p>{key.toUpperCase()}:{value.toString()}</p>
                            })

                        }
                    </div>
            </div>
        </div>
    )
}

export const SwDetail = () => {
    return (
        <LeftColumn />
    )
}