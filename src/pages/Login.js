import React, {useState} from "react";
import {
    useHistory,
    useLocation
} from 'react-router-dom'
import {Input} from '../items/Input'
import {usersData, fakeAuth} from '../data/users'

export const Login = () => {
    const [values, setValues] = useState({})

    let history = useHistory();
    let location = useLocation();
    let {from} = location.state || {from: {pathname: "/"}};

    const changeInput = ({name, event}) => {
        values[name] = event.target.value
        setValues(values)
    }

    const submit = (e) => {
        const username = values.username,
            password = values.password;

        if (usersData.username === username && usersData.password === password) {
            fakeAuth.authenticate(() => {
                history.replace(from);
            });

        } else {
            alert('ERROR.');
        }
    }

    return (
        <div className="absolute-center">
            <div className="login">
                <form onSubmit={(e) => {
                    e.preventDefault()
                    submit(e)
                }}>
                    <h4>Login</h4>
                    <Input type="text" name='username' placeholder='Username' change={changeInput}/>
                    <Input type="password" name='password' placeholder='Password' change={changeInput}/>
                    <button type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    )
}