import React from 'react';
import {Header} from "../items/Header";
import {Footer} from "../items/Footer";
import {RightColumn} from "../items/RightColumn";


const LeftColumn = () => {
    return (
        <div className="left">
            <div className="block">
                <h1>Star Wars</h1>
                <span>From Wikipedia, the free encyclopedia</span>
                <p>
                    Star Wars is an American epic space-opera media franchise created by George Lucas, which
                    began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon.
                    The franchise has been expanded into various films and other media, including television
                    series, video games, novels, comic books, theme park attractions, and themed areas,
                    comprising an all-encompassing fictional universe.[b] The franchise holds a Guinness World
                    Records title for the "Most successful film merchandising franchise".[2] In 2020, the total
                    value of the Star Wars franchise was estimated at US$70 billion, and it is pop-culturerently
                    the fifth-highest-grossing media franchise of all time.
                </p>
                <p>
                    The original film, retroactively subtitled Episode IV: A New Hope, was followed by the
                    sequels Episode V: The Empire Strikes Back (1980) and Episode VI: Return of the Jedi (1983),
                    forming the original Star Wars trilogy. A prequel trilogy was later released, consisting of
                    Episode I: The Phantom Menace (1999), Episode II: Attack of the Clones (2002), and Episode
                    III: Revenge of the Sith (2005). In 2012, Lucas sold his production company to Disney,
                    relinquishing his ownership of the franchise. The subsequently produced sequel trilogy
                    consists of Episode VII: The Force Awakens (2015), Episode VIII: The Last Jedi (2017), and
                    Episode IX: The Rise of Skywalker (2019). Together, the three trilogies form what has been
                    referred to as the "Skywalker saga". All nine films were nominated for Academy Awards (with
                    wins going to the first two released) and were commercially successful. Together with the
                    theatrical spin-off films Rogue One (2016) and Solo: A Star Wars Story (2018), the combined
                    box office revenue of the films equates to over US$10 billion,[3] and it is currently the
                    second-highest-grossing film franchise.[4]
                </p>
            </div>

            <div className="block">
                <h1>Premise</h1>
                <p>
                    The Star Wars franchise depicts the adventures of characters "A long time ago in a galaxy
                    far, far away",[5] in which humans and many species of aliens (often humanoid) co-exist with
                    robots, or 'droids', who may assist them in their daily routines; space travel between
                    planets is common due to hyperspace technology,[6][7][8] and spacecraft range from
                    airplane-like starfighters, such as the tiny TIE fighters, to huge capital ships, such as
                    the "terrifyingly large" Star Destroyers, and gargantuan space stations, most notably the
                    moon-sized Death Stars.
                </p>
                <p>
                    A mystical power known as 'the Force' is described in the original film as "an energy field
                    created by all living things ... [that] binds the galaxy together."[9] Through training and
                    meditation, those whom "the Force is strong with" are able to perform various superpowers
                    (such as telekinesis, precognition, telepathy, and manipulation of physical energy).[10] The
                    Force is wielded by two major knighthood orders at conflict with each other: the Jedi,
                    peacekeepers of the Republic who act on the light side of the Force through non-attachment
                    and arbitration, and the Sith, ancient enemies of the galactic democracy, who use the dark
                    side by manipulating fear and aggression. While Jedi Knights can be numerous, the Dark Lords
                    of the Sith (or 'Darths') are intended to be limited to two: a master and their
                    apprentice.[11]
                </p>
                <p>
                    Force-wielders are very limited in numbers in comparison to the rest of the average
                    population. The Jedi and Sith prefer the use of a weapon called lightsaber, which is the
                    cylinder-like hilt of a sword (when turned off), but when turned on ignites a blade of
                    energy that can cut through virtually any surface. Battles between the two factions result
                    in duels, which are a mix between sword skills and the use of the Force. The rest of the
                    average population, as well as renegades and soldiers, use laser-powered blaster firearms,
                    the deadly beams of which Force users can deflect using lightsabers.
                </p>
                <p>
                    In the outer reaches of the galaxy, the underworld is also extremely developed, with [Crime
                    boss|[crime lord]]s, often belonging to the Hutt species, administrating powerful Mafia
                    clans, and Western-style bounty hunters who are often employed by both gangsters and
                    governments.
                </p>
            </div>
        </div>
    )
}

export const Home = () => {
    return (
        <div>
            <Header/>
            <div className="content">
                <LeftColumn />
                <RightColumn/>
            </div>

            <Footer/>
        </div>
    );
}
