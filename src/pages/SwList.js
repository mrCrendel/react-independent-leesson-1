import React, {useEffect, useCallback, useState} from "react";
import {useSelector, useDispatch} from 'react-redux';
import {useParams, useRouteMatch, Switch, Route} from "react-router-dom";

//items
import {Header} from "../items/Header";
import {RightColumn} from "../items/RightColumn";
import {Footer} from "../items/Footer";
import {SwListItem} from "../items/SwListItem";

// actions
import {
    loadSwData,
    showSwListDetailAction,
    deleteSwItemAction,
    searchSwItemAction,
    addNewSwAction
} from "../actions/sw.actions"

//pages
import {SwDetail} from "./SwDetail"
import {Input} from "../items/Input";
import {fakeAuth, usersData} from "../data/users";


const LeftColumn = () => {
    const dataList = useSelector(state => state.sw);
    const dispatch = useDispatch();

    const [values, setValues] = useState({})

    let {slug} = useParams();
    let {path, url} = useRouteMatch();

    const showDetail = useCallback((id) => {
        dispatch(showSwListDetailAction(id));
    })

    const deleteDetail = useCallback((id) => {
        dispatch(deleteSwItemAction(id));
    })

    const searchDetail = useCallback((e) => {
        const {key, target} = e
        if (key === 'Enter') {
            dispatch(searchSwItemAction(target.value));
        }
    })

    const changeInput = ({name, event}) => {
        values[name] = event.target.value
        setValues(values)
    }

    const submit = (e) => {
        console.log(values)
        dispatch(addNewSwAction(values))
        
    }

    useEffect(() => {
        dispatch(loadSwData(slug));
    }, []);

    return (
        <div className="left">
            <div className="block-header">
                <h1>{url.split('/')[1].toUpperCase()} items</h1>
            </div>
            <div className="search">
                <input
                    onKeyDown={searchDetail}
                    type="text" name="search"
                    placeholder="Search..."/>
            </div>
            {
                dataList.data.map(item => {
                    if (item.isVisible && !item.isRemoved) {
                        return <SwListItem
                            detail={showDetail}
                            remove={deleteDetail}
                            item={item}
                            url={url}/>
                    }
                })
            }

            <div className="block">
                <form onSubmit={(e) => {
                    e.preventDefault()
                    submit(e)
                }}>
                    <Input type="text" name='name' placeholder='Name' change={changeInput}/>
                    <Input type="text" name='description' placeholder='Description' change={changeInput}/>
                    <button>submit</button>
                </form>

            </div>
        </div>
    )
}

export const SwList = () => {
    let {url} = useRouteMatch();
    let {slug} = useParams();

    return (
        <div>
            <Header/>
            <div className="content">
                <Switch>
                    <Route path={`${url}/:id`}>
                        <SwDetail/>
                    </Route>
                    <Route>
                        <LeftColumn/>
                    </Route>
                </Switch>
                <RightColumn/>
            </div>

            <Footer/>
        </div>
    )
}