import {
    SW_LIST_ADD,
    SW_LIST_ADD_NEW,
    SW_DETAIL_ADD,
    SW_LIST_SHOW_DETAIL,
    SW_LIST_DELETE_ITEM,
    SW_LIST_SEARCH_ITEM,
} from '../actions/sw.actions'

const initialState = {
    data: []
};

export const swReducer = (state = initialState, action) => {
    switch (action.type) {
        case SW_LIST_ADD:
            const dataNew = action.payload.map((item, index) => {
                const utlLength = item.url.split('/')
                return {
                    ...item,
                    ...{
                        id: utlLength[utlLength.length - 2],
                        isVisible: true,
                        isActive: true,
                        isRemoved: false,
                        showDetail: false
                    }
                }
            })
            return {...state, data: dataNew};

        case SW_LIST_ADD_NEW:
            const copyAddNewState = state.data.slice();
            copyAddNewState.push(
                Object.assign(action.payload, {
                    isVisible: true,
                    isActive: true,
                    isRemoved: false,
                    showDetail: false
                }));

            return {...state, data: copyAddNewState};

        case SW_LIST_SHOW_DETAIL:
            const copyData = state.data.slice();
            const data = copyData.map((item) => {
                if (item.id === action.payload) {
                    return {...item, ...{showDetail: !item.showDetail}}
                } else {
                    return item
                }
            })
            return {...state, data: data};

        case SW_LIST_DELETE_ITEM:
            const copyDataDelete = state.data.slice();
            const dataDelete = copyDataDelete.map((item) => {
                if (item.id === action.payload) {
                    return {...item, ...{isRemoved: !item.isRemoved}}
                } else {
                    return item
                }
            })
            return {...state, data: dataDelete};

        case SW_LIST_SEARCH_ITEM:
            const copySearchData = state.data.slice()
            const searchData = copySearchData.map(item => {
                const searchValue = item.name ? item.name : item.title
                if (searchValue.includes(action.payload)) {
                    return {...item, ...{isVisible: true}}
                } else {
                    return {...item, ...{isVisible: false}}
                }
            })
            return {...state, data: searchData};

        case SW_DETAIL_ADD:
            return {
                ...action.payload,
                ...{
                    isVisible: true,
                    isActive: true,
                    isRemoved: false,
                    showDetail: false
                }
            }


        default:
            return state
    }
}