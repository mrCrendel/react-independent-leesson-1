import {combineReducers, createStore, applyMiddleware} from 'redux';
import {swReducer} from './sw.reducer'

const reducer = combineReducers({
  sw: swReducer
})

export default reducer