import React, {memo} from "react";
import {Link} from "react-router-dom"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

export const SwListItem = memo(({item, url, detail, remove}) => {

    const element = <FontAwesomeIcon onClick={() => remove(item.id)} icon={faTrashAlt} />
    return (
        <div className="block">
            <Link to={`${url}/${item.id}`}>{item.name ? item.name : item.title}</Link>
            <hr/>
            {element}

            <div className={item.showDetail ? "detail active" : "detail"}>
                {
                    Object.entries(item).map(([key, value]) => {
                        return <p>{key.toUpperCase()}:{value.toString()}</p>
                    })
                }
            </div>

            <span onClick={() => detail(item.id)}>
                {item.showDetail ? "show less" : "show more"}
            </span>
        </div>
    )
})