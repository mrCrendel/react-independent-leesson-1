import React from 'react';

export function Input({type, placeholder, change, name}){

    return <div>
        <input
            type={type}
            name={name}
            placeholder={placeholder}
            onChange={e => change({name, event: e})}
        />
    </div>
}