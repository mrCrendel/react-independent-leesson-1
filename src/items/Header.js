import React from "react";
import {useHistory} from 'react-router-dom';
import {fakeAuth} from '../data/users'


const Logout = () => {
    let history = useHistory();
    return fakeAuth.isAuthenticated ?
        <li className="log-out">
            <a onClick={() => {
                fakeAuth.signout(() => history.push("/"));
            }}
               href="/">Log out
            </a>
        </li>: <li className="log-out">
            <a href="/login">Login
            </a>
        </li>
}


export const Header = () => {

    return (
        <ul>
            <li><a href="/">Home</a></li>
            <li className="dropdown">
                <a href="javascript:void(0)" className="dropbtn">Themes</a>
                <div className="dropdown-content">
                    <a href="/people">People</a>
                    <a href="/planets">Planets</a>
                    <a href="/films">Films</a>
                    <a href="/species">Species</a>
                    <a href="/vehicles">Vehicles</a>
                    <a href="/starships">Starships</a>
                </div>
            </li>
            <Logout/>

        </ul>
    )
}