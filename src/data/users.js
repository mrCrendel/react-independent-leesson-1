export const usersData = {
    "username": "admin",
    "password": "admin"
}

export const fakeAuth = {
  isAuthenticated: localStorage.getItem("authorised") === 'true',
  authenticate(cb) {
    localStorage.setItem("authorised", "true");
    fakeAuth.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },
  signout(cb) {
    localStorage.setItem("authorised", "false");
    fakeAuth.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};